<?php
spl_autoload_register(
/**
 * @param $name
 */
    function ($name) {

        $fileName = str_replace('\\', '/', $name . '.php');
        if (!empty($fileName)) {
            include $fileName;
        }
    });