<?php

use Task3\Image;
use Task3\CommentComposite;
use Task3\TextBlock;
use Task3\Button;
use Task3\PostComposite;

require_once 'Utils/autoload.php';

$image = new Image();
$commentBlock = new CommentComposite();
$text = new TextBlock();
$button = new Button();
$commentBlock
    ->add($image, 'comment_image')
    ->add($text, 'comment_text')
    ->add($button, 'comment_button')    
;
$postBlock = new PostComposite();
$postBlock
    ->add($text, 'post_text')
    ->add($commentBlock, 'post_comment');
echo $postBlock->renderComposition();
echo '<pre>';
$postBlock->displayStructure();
echo '</pre>';