<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:18
 */

namespace Task4\Composite;


use Task4\Iterator\CompositeIterator;

class BlockComposite extends Component
{
    protected $_iterator = null;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function add(Component $child)
    {
        if(is_null($this->children)){
            $this->children = new \ArrayObject();
        }
        $this->children->append($child);
        return $this;
    }

    public function getIterator()
    {
        if(is_null($this->_iterator)){
            /** @var \ArrayObject $children */
            $children = $this->children;
            $this->_iterator = new CompositeIterator($children->getIterator());
        }
        return $this->_iterator;
    }
    
    public function getFilteredIterator(){
        
    }
}