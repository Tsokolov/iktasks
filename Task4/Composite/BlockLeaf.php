<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:15
 */

namespace Task4\Composite;


use Task4\Iterator\LeafIterator;

class BlockLeaf extends Component
{
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function add(Component $child)
    {
        throw new \Exception('You can not add children to leaf');
    }

    public function renderComposition()
    {
        return null;
    }

    public function getIterator()
    {
        return new LeafIterator();
    }
}