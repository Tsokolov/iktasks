<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 15:31
 */

namespace Task4\Iterator;


use Task4\Composite\Component;

interface IIterator
{
    /**
     * @return boolean check current element existence
     */
    public function valid();

    /**
     * @return mixed set cursor next
     */
    public function next();

    /**
     * @return Component get current element
     */
    public function current();

}