<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 15:26
 */

namespace Task4\Iterator;


class LeafIterator implements IIterator
{
    /**
     * @return boolean check current element existence
     */
    public function valid()
    {
        return false;
    }

    /**
     * @return mixed set cursor next
     */
    public function next()
    {
        return false;
    }

    /**
     * @return mixed get current element
     */
    public function current()
    {
        return null;
    }
}