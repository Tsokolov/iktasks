<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 15:24
 */

namespace Task4\Iterator;


use Task4\Composite\BlockComposite;
use Task4\Composite\Component;

class CompositeIterator implements IIterator
{
    public $stack = [];

    public function __construct($iterator)
    {
        $this->stack[] = $iterator;
    }

    /**
     * @return boolean check current element existence
     */
    public function valid()
    {
        if (empty($this->stack)) {
            return false;
        } else {
            /** @var \ArrayIterator $iterator */
            $iterator = array_shift(array_values($this->stack));
            if ($iterator->valid()) {
                return true;
            } else {
                array_shift($this->stack);
                return $this->valid();
            }
        }
    }

    /**
     * @return mixed set cursor next
     */
    public function next()
    {
        /** @var \ArrayIterator $iterator */
        $iterator = current($this->stack);
        /** @var Component $component */
        $component = $iterator->current();
        if ($component instanceof BlockComposite) {
            foreach ($component->getChildren() as $child) {
                /** @var Component $child */
                $child->setLevel($component->getLevel() + 1);
            }
            array_push($this->stack, $component->getChildren()->getIterator());
        }
        $iterator->next();
    }

    /**
     * @return Component get current element
     */
    public function current()
    {
        if ($this->valid()) {
            /** @var \ArrayIterator $iterator */
            $iterator = array_shift(array_values($this->stack));
            return $iterator->current();
        } else {
            return null;
        }
    }
}