<?php

namespace Task1;

class PairBlock extends Block
{
    protected $content;

    public function render()
    {
        return "<$this->name".$this->buildAttributes().'>'.$this->content."</$this->name>";
    }
}