<?php

namespace Task1;

class Image extends SingleBlock{

    public function __construct($attributes=[])
    {
        $this->name = 'img';
        $this->setAttributes($attributes);
        $this->pair = false;
    }
}