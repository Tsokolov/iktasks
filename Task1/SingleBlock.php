<?php

namespace Task1;


class SingleBlock extends Block
{
    public function render()
    {
        return '<'.$this->name . $this->buildAttributes().'/>';
    }
}