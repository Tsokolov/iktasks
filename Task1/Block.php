<?php

namespace Task1;

abstract class Block{

    protected $name;
    protected $attributes;

    abstract public function render();

    /**
     * @param mixed $attributes
     */
    public function setAttributes($attributes)
    {
        foreach ($attributes as $k=>$v){
            $this->attributes[$k] = $v;
        }
    }

    /**
     * @return string
     */
    protected function buildAttributes(){
        $pairs = [];
        foreach ($this->attributes as $k=>$v){
            $pairs[] = "$k=\"$v\"";
        }
        return (count($pairs)>0)?' '.implode(' ', $pairs):'';
    }

    public function getLength(){
        return strlen($this->render());
    }

}