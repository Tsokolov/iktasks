<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 23.05.2016
 * Time: 17:01
 */

namespace Task1;


class Button extends PairBlock
{

    public function __construct($text, array $attributes)
    {
        $this->name = 'button';
        $this->text = $text;
        $this->setAttributes($attributes);
    }
}