<?php

namespace Task1;


class Text extends PairBlock
{
    public function __construct($content = null, array $attributes = [])
    {
        $this->name = 'p';
        $this->content = $content;
        $this->setAttributes($attributes);
    }
}