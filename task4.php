<?php

use Task4\Composite\BlockComposite;
use Task4\Composite\BlockLeaf;

include 'Utils/autoload.php';

$c1 = new BlockComposite('C1L0');
$l1 = new BlockLeaf('L1L0');
$l2 = new BlockLeaf('L2L0');
$c1->add($l1);
$c1->add($l2);
$c2 = new BlockComposite('C2L0');
$c3 = new BlockComposite('C2L1');
$l4 = new BlockLeaf('L4L2');
$c3->add($l4);
$l3 = new BlockLeaf('L3L1');
$c2->add($l3);
$c1->add($c2);
$c2->add($c3);

$iterator = $c1->getIterator();
$i = 1;
while ($iterator->valid()) {
    $component = $iterator->current();
    if($component->getLevel()>=1){
        $component->setNumber($i);
        $component->setClassName((new ReflectionClass($component))->getShortName());
    }
    $iterator->next();
    $i++;
}
//while ($iterator->valid()) {
//    $component = $iterator->current();
//    $component->setNumber($i);
//    $component->setClassName((new ReflectionClass($component))->getShortName());
//    $iterator->next();
//    $i++;
//}
echo '<pre>'.print_r($c1->getChildren(), true).'</pre>';