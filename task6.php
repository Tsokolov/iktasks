<?php

use Task6\AbstractBlock;

include 'Utils/autoload.php';

$abstractBlock = new AbstractBlock();
$abstractBlock->setPrefix('<h1>Prefix</h1>');
$abstractBlock->setPostfix('<h1>Postfix</h1>');
echo $abstractBlock->templateMethod();