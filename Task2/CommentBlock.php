<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 23.05.2016
 * Time: 18:28
 */

namespace Task2;


class CommentBlock extends BlockDecorator
{
    public function render()
    {
        return '<!-- Block BEGIN. Type: ' . get_class($this->block) . ', ID: ' . spl_object_hash($this->block) . ', Length: ' . $this->block->getLength() . ' -->'
        . $this->block->render() .
        '<!-- Block END. Type: ' . get_class($this->block) . ', ID: ' . spl_object_hash($this->block) . ' -->';
    }
}