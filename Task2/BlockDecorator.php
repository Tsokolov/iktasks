<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 23.05.2016
 * Time: 18:26
 */

namespace Task2;

use Task1\Block;

abstract class BlockDecorator extends Block
{
    protected $block;

    public function __construct(Block $block)
    {
        $this->block = $block;
    }
}