<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 23.05.2016
 * Time: 18:56
 */

namespace Task2;


class BorderBlock extends BlockDecorator
{
    protected $thickness;
    protected $color;

    public function __construct($block, $thickness, $color)
    {
        parent::__construct($block);
        $this->color = $color;
        $this->thickness = $thickness;
    }

    public function render()
    {
        $style = "border: $this->thickness"."px solid $this->color;";
        $attributes = [];
        $attributes['style'] = $style;
        $this->block->setAttributes($attributes);
        return $this->block->render();
    }
}