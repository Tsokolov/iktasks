<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:18
 */

namespace Task3\Pattern;


class BlockComposite extends Component
{
    public function add(Component $child, $namespace)
    {
        $child->addParent($this);
        $this->children[$namespace] = $child;
        return $this;
    }

    public function addParent(Component $parent)
    {
        parent::addParent($parent);
    }

    public function renderComposition()
    {
        return null;
    }

    public function renderPlaceHolder($namespace)
    {
        /** @var Component $placeholder */
        $placeholder = $this->children[$namespace];
        return $placeholder->renderComposition();
    }

    public function displayStructure(&$counter = 0, $level=0)
    {
        foreach ($this->children as $namespace => $child) {
            $counter++;
            $prefix = '';
            for($j=0;$j<$level;$j++){
                $prefix.="\t";
            }
            echo "$prefix $counter - " . get_class($child) . " [$namespace]\n";
            if($child instanceof BlockComposite){
                $child->displayStructure($counter, $level+1);
            }
        }
    }
}