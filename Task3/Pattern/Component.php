<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:11
 */

namespace Task3\Pattern;


abstract class Component
{
    protected $children = [];
    protected $parent = null;

    abstract public function add(Component $child, $namespace);

    abstract public function renderComposition();

    protected function addParent(Component $parent)
    {
        $this->parent = $parent;
    }

    public function getChildren(){
        return $this->children;
    }
}