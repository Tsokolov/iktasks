<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 16:21
 */

namespace Task3;


use Task3\Pattern\BlockComposite;

class PostComposite extends BlockComposite
{
    public function renderComposition()
    {
        return '<div>'.$this->renderPlaceHolder('post_text').$this->renderPlaceHolder('post_comment').'</div>';
    }
}