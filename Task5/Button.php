<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:55
 */

namespace Task5;


use Task5\Composite\BlockComposite;

class Button extends BlockComposite
{
    public function renderComposition()
    {
        return '<button type="submit">Submit</button>';
    }
}