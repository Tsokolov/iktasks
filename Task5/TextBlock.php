<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:54
 */

namespace Task5;

use Task5\Composite\BlockComposite;

class TextBlock extends BlockComposite
{
    public function renderComposition()
    {
        return '<p>Lorem ipsum dolor sit amet, bibendum ac vitae turpis massa, ipsum ac euismod vel lacus blandit, illo nibh fermentum faucibus etiam ac quis, mauris amet repudiandae. Potenti arcu bibendum vivamus. Justo suspendisse augue eu quisque cumque eget, velit nulla, in commodo nostra. Curabitur est eu nibh, tempor litora quisque officia fermentum erat. Eros accumsan nam est quam, quis orci mauris eu integer consequat praesent, blandit sit. Sit duis massa nunc sodales nonummy elit, ut eros eget tortor eu lacus. Sed nunc, felis magnis nulla, etiam id velit neque urna, lobortis facilisi aliquet.</p>';
    }
}