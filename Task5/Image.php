<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:42
 */

namespace Task5;

use Task5\Composite\BlockLeaf;

class Image extends BlockLeaf
{
    public function renderComposition()
    {
        return '<img src="http://websoft.in.ua/images/logo.png" width="200"/>';
    }
}