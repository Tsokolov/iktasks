<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 21:31
 */

namespace Task5\Pattern;

use ReflectionClass;
use Task5\Composite\Component;

class CommentTemplateMethod extends TemplateMethod
{

    protected function RenderPrefix()
    {
        return '<!-- Block BEGIN. ID: ' . spl_object_hash($this->component) . ', Class name: ' . (new ReflectionClass($this->component))->getShortName() . ' -->';
    }

    protected function RenderPostfix()
    {
        $pluginsNames = [];
        /** @var Component $component */
        $component = $this->component;
        foreach ($component->getPlugins() as $plugin) {
            $pluginsNames[] = (new ReflectionClass($plugin))->getShortName();
        }
        $plugins = implode(', ', $pluginsNames);
        return '<!-- Block END. Length: ' . strlen($component->renderComposition()) . ', Plugins: ' . $plugins . ' -->';
    }

    protected function renderBlock()
    {
        /** @var Component $component */
        $component = $this->component;
        return $component->renderComposition();
    }
}