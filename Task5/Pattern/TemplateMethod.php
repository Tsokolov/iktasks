<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 21:24
 */

namespace Task5\Pattern;


use Task5\Composite\Component;

abstract class TemplateMethod
{
    protected $component;

    /**
     * @param Component $component
     */
    public function setComponent(Component $component)
    {
        $this->component = $component;
    }

    final public function applyPlugin()
    {
        echo $this->renderPrefix();
        echo $this->renderBlock();
        echo $this->renderPostfix();
    }

    abstract protected function renderBlock();

    abstract protected function renderPrefix();

    abstract protected function renderPostfix();
}