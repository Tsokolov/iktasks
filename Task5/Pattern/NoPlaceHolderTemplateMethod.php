<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 22:33
 */

namespace Task5\Pattern;


use Task5\Composite\Component;

class NoPlaceHolderTemplateMethod extends TemplateMethod
{

    protected function renderBlock()
    {
        // TODO: Implement renderBlock() method.
    }

    protected function renderPrefix()
    {
        // TODO: Implement renderPrefix() method.
    }

    protected function renderPostfix()
    {
        /** @var Component $component */
        $component = $this->component;
        foreach ($component->getChildren() as $namespace => $child) {
            if (is_int($namespace)) {
                /** @var Component $child */
                echo $child->renderComposition();
            }
        }
    }
}