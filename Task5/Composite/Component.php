<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:11
 */

namespace Task5\Composite;


use Task5\Pattern\TemplateMethod;

abstract class Component
{
    protected $children = [];
    protected $plugins = [];
    protected $parent = null;

    public function addPlugin($class)
    {
        /** @var TemplateMethod $plugin */
        $plugin = new $class;
        $plugin->setComponent($this);
        $this->plugins[] = $plugin;
    }

    public function applyPlugins()
    {
        foreach ($this->plugins as $plugin) {
            /** @var TemplateMethod $plugin */
            $plugin->applyPlugin();
        }
    }

    abstract public function add(Component $child, $namespace);

    abstract public function renderComposition();

    /**
     * @return array
     */
    public function getPlugins()
    {
        return $this->plugins;
    }

    protected function addParent(Component $parent)
    {
        $this->parent = $parent;
    }

    public function getChildren()
    {
        return $this->children;
    }
}