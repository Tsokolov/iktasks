<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:15
 */

namespace Task5\Composite;


class BlockLeaf extends Component
{

    public function add(Component $child, $namespace)
    {
        throw new \Exception('You can not add children to leaf');
    }

    public function renderComposition()
    {
        return null;
    }
}