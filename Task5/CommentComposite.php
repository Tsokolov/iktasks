<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 24.05.2016
 * Time: 15:44
 */

namespace Task5;

use Task5\Composite\BlockComposite;


class CommentComposite extends BlockComposite
{
    public function renderComposition()
    {
        return '<div>' . $this->renderPlaceHolder('comment_image') . $this->renderPlaceHolder('comment_text') . '</div>';
    }
}