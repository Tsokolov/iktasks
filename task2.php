<?php

use Task1\Image;
use Task1\Text;
use Task2\CommentBlock;
use Task2\BorderBlock;

require_once 'Utils/autoload.php';

$document = [];

$image = new Image(
    [
        'src' => 'http://websoft.in.ua/images/logo.png',
        'width' => '200'
    ]);

$document[] = new CommentBlock(new Text("Hello world"));
$borderedImage = new BorderBlock($image, 1, 'red');
$commentedBorderedImage = new CommentBlock($borderedImage);
$document[] = $commentedBorderedImage;

foreach ($document as $element){
    echo $element->render();
}