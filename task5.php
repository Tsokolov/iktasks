<?php

use Task5\Image;
use Task5\Pattern\CommentTemplateMethod;
use Task5\Pattern\NoPlaceHolderTemplateMethod;
use Task5\CommentComposite;
use Task5\TextBlock;

include 'Utils/autoload.php';

$image = new Image();
$text = new TextBlock();
$text2 = new TextBlock();
$comment = new CommentComposite();
$comment->add($image, 'comment_image');
$comment->add($text, 'comment_text');
$comment->add($text2);
$comment->add($text2);
$comment->addPlugin(CommentTemplateMethod::class);
$comment->addPlugin(NoPlaceHolderTemplateMethod::class);
$comment->applyPlugins();