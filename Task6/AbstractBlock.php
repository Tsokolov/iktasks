<?php
/**
 * Created by PhpStorm.
 * User: SERGEY
 * Date: 25.05.2016
 * Time: 23:00
 */

namespace Task6;


use Task5\CommentComposite;

class AbstractBlock extends CommentComposite
{
    protected $prefix = null;
    protected $postfix = null;

    public function renderComposition()
    {
        return '<div><h1>This is abstract block</h1></div>';
    }

    public function templateMethod()
    {
        return $this->prefix.$this->renderComposition().$this->postfix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @param string $postfix
     */
    public function setPostfix($postfix)
    {
        $this->postfix = $postfix;
    }
}